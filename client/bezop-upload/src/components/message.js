import React from "react";

const MessageInfo = (props) => {
  return (
    <div className={props.info.success ? 'msg-info valid' : 'msg-info invalid'}>
      <div className="message">{props.info.message}</div>
    </div>
  )
}

export default MessageInfo;
