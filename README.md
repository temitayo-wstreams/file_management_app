




### This application allows users to 
  - Register for an account, 

  - upload files, 

  - transfer files to trash

  - access both saved and trashed files 

#Running app in development 
  1. Clone the repository:
  ```
  git clone https://temitayo-wstreams@bitbucket.org/temitayo-wstreams/file_management_app.git
  ```
  2. Navigate into the cloned repository folder

  3. Install dependencies:
  ```
  $ npm install
  ```
  4. run `npm start` to start the server

  5. visit `http://localhost:8000` to use the app




