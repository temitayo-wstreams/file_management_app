const express = require('express'),
	app = express(),
	bodyParser = require('body-parser'),
	path = require('path'),
	http = require('http').Server(app);
  const format = require('util').format;
  const Multer = require('multer');




  const Storage = require('@google-cloud/storage');

  // Instantiate a storage client
  const storage = Storage();
  app.set('view engine', 'pug');
  app.use(bodyParser.json());
  
  // Multer is required to process file uploads and make them available via
  // req.files.
  const multer = Multer({
    storage: Multer.memoryStorage(),
    limits: {
      fileSize: 5 * 1024 * 1024 // no larger than 5mb, you can change as needed.
    }
  });
  
  // A bucket is a container for objects (files).
  // const bucket = storage.bucket(process.env.GCLOUD_STORAGE_BUCKET);




// // Process the file upload and upload to Google Cloud Storage.
// app.post('/add-file', multer.single('file'), (req, res, next) => {
//   if (!req.file) {
//     res.status(400).send('No file uploaded.');
//     return;
//   }

//   // Create a new blob in the bucket and upload the file data.
//   const blob = bucket.file(req.file.originalname);
//   const blobStream = blob.createWriteStream();

//   blobStream.on('error', (err) => {
//     next(err);
//   });

//   blobStream.on('finish', () => {
//     // The public URL can be used to directly access the file via HTTP.
//     const publicUrl = format(`https://storage.googleapis.com/${bucket.name}/${blob.name}`);
//     res.status(200).send(publicUrl);
//   });

//   blobStream.end(req.file.buffer);
// });

// loading env variables
require('dotenv').config();

app.disable('x-powered-by');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));




app.use(express.static(path.join(__dirname,  './build')));
app.use(express.static(path.join(__dirname,  './uploads')));
// app.use(express.static(path.join(__dirname + './../template')));

if(process.env.NODE_ENV != "production"){
	const morgan = require('morgan');
  app.use(morgan('dev'));
}

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,PATCH");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

//load all routes
app.use(require('./routes/index'));

app.set('port', process.env.PORT || 8000);

// connecting to database
require("./models/database");

http.listen(app.get('port'), function (err) {
  if (!err) console.log('server listening on port ', app.get('port'));
  else console.log(err);
});


