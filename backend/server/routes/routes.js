const express = require("express");
const route = express.Router();
const Multer  = require('multer')
const ImgUpload=require("../utils/ImgUpload")
// var upload = null;

const upload = Multer({
  storage: Multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024 
  },
});
// }


const MainCtrl = require("../controllers/main-ctrl");
const  authenticate  = require("../middlewares/authenticate");

const { 
  signin, 
  signup, 
  addFile, 
  removeFile, 
  retrieveFiles, 
  undoRemoveFile, 
} = MainCtrl;


// USERS AUTHENTICATION
route.post('/signin', signin);


// USERS REGISTRATION
route.post('/signup', signup);


// ADD, REMOVE AND RETRIEVE FILES
route.post('/add-file', authenticate, upload.single('avatar'), addFile);
route.put("/remove-file", authenticate, removeFile);
route.put("/undo-remove-file", authenticate, undoRemoveFile);
route.get("/retrieve-files", authenticate, retrieveFiles);



module.exports = route;
